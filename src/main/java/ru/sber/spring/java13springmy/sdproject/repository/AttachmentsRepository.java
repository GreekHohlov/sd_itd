package ru.sber.spring.java13springmy.sdproject.repository;

import ru.sber.spring.java13springmy.sdproject.model.Attachments;

public interface AttachmentsRepository extends GenericRepository<Attachments>{
}
