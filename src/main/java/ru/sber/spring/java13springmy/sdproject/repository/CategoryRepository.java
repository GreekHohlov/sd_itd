package ru.sber.spring.java13springmy.sdproject.repository;

import ru.sber.spring.java13springmy.sdproject.model.Category;

public interface CategoryRepository extends GenericRepository<Category>{
}
