package ru.sber.spring.java13springmy.sdproject.repository;

import ru.sber.spring.java13springmy.sdproject.model.WorkSchedule;

public interface WorkScheduleRepository extends GenericRepository<WorkSchedule>{
}
