package ru.sber.spring.java13springmy.sdproject.repository;

import ru.sber.spring.java13springmy.sdproject.model.TypeTask;

public interface TypeTaskRepository extends GenericRepository<TypeTask>{
}
